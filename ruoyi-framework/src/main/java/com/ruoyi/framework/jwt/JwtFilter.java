package com.ruoyi.framework.jwt;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
 
/**
 * @author dongpeng
 * @create 2018-07-12 15:56
 * @desc
 **/
public class JwtFilter extends BasicHttpAuthenticationFilter {
 
 
    /**
     * 执行登录认证
     *
     * @param request
     * @param response
     * @param mappedValue
     * @return
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        try {
            executeLogin(request, response);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
 
    /**
     *
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String token = httpServletRequest.getHeader("Authorization");
 
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("jwtTokenUserName",token);
        // 提交给realm进行登入，如果错误他会抛出异常并被捕获
        getSubject(request, response).login(usernamePasswordToken);
        // 如果没有抛出异常则代表登入成功，返回true
        return true;
    }
}
